import { createElementFromHtml } from '../../utils/htmlConverter';

export default class CredentialsRow {
  constructor(login = '', password = '', host) {
    const hostRow = host !== undefined
      ? `<div class="td"><input type="text" data-field="host" value="${host}" maxlength="255" placeholder="Hostname"></div>`
      : '';
    const html = `<div class="tr">
      ${hostRow}
      <div class="td"><input type="text" data-field="login" value="${login}" placeholder="Username"></div>
      <div class="td"><input type="password" data-field="password" value="${password}" placeholder="Password"></div>
      <div class="td delete-col" title="Remove entry"><span class="ql-icon no-padding delete">🗑</span></div>
    </div>`;
    this._el = createElementFromHtml(html);
  }

  init() {
    this._el.querySelectorAll('input').forEach(input => {
      input.addEventListener('input', () => document.dispatchEvent(new Event('ql-config-changed')));
    });
    this._el.querySelector('.delete').addEventListener('click', () => {
      this._el.remove();
      document.dispatchEvent(new Event('ql-config-changed'));
    });

    return this._el;
  }
}