import chromeStorage from '../utils/chromeStorage';
import CredentialsRow from './components/credentialsRow';

export default class CredentialsTableUtils {
  constructor(table) {
    this._table = table;
  }

  async refreshTable(isSettings = false) {
    let config = await chromeStorage.get('ql-config') || [];

    this._table._rowsContainer.innerHTML = '';
    if (!isSettings) {
      config = config.filter(item => item.host === window.location.host);
    }
    config.forEach(item => {
      this._table._rowsContainer.appendChild(new CredentialsRow(item.login, item.password, isSettings ? item.host : undefined).init());
    });
    this.checkTableIsEmpty();
    this._table._addBtn.removeAttribute('disabled');
  }

  checkTableIsEmpty() {
    if (this._table._rowsContainer.children.length === 0) {
      this._table._rowsContainer.innerHTML = this._getTextRowHtml('Nothing is stored.');
    }
  }

  addRow(login = '', password = '', host) {
    if (this._table._rowsContainer.innerHTML.includes('Nothing is stored.')) {
      this._table._rowsContainer.innerHTML = '';
    }
    this._table._rowsContainer.appendChild(new CredentialsRow(login, password, host).init());
    this.enableSaveBtn();
  }

  validateRow(row) {
    let ok = true;
    row.querySelectorAll('input').forEach(inp => {
      if (inp.value.trim() === '') {
        inp.classList.add('required');
        ok = false;
      } else {
        if (inp.classList.contains('required')) inp.classList.remove('required');
      }
    });
    return ok;
  }

  enableSaveBtn() {
    this._table._saveBtn.removeAttribute('disabled');
    this._table._saveBtn.innerHTML = '<span class="ql-icon no-padding">💾</span> Save';
  }

  _getTextRowHtml(text) {
    return `<div class="tr only-text">${text}</div>`;
  }
}