const get = key => {
  return new Promise((resolve, reject) => {
    chrome.storage.local.get([key], items => {
      if (chrome.runtime.lastError) {
        console.error(chrome.runtime.lastError);
        reject(chrome.runtime.lastError.message);
      } else {
        resolve(items[key]);
      }
    });
  });
};

const set = (key, value) => {
  return new Promise((resolve, reject) => {
    chrome.storage.local.set({ [key]: value }, () => {
      if (chrome.runtime.lastError) {
        console.error(chrome.runtime.lastError);
        reject(chrome.runtime.lastError.message);
      } else {
        resolve();
      }
    });
  });
};

export default {
  get,
  set
};
