import { createElementFromHtml } from '../../utils/htmlConverter';
import chromeStorage from '../../utils/chromeStorage';
import CredentialsTableUtils from '../../shared/credentialsTableUtils';

export default class Settings {
  constructor() {
    const settingsHtml = `
      <div class="ql-settings-wrapper">
        <div class="controls">
            <button class="btn add-btn" disabled><span class="ql-icon no-padding">➕</span> Add entry</button>
            <div class="copy-group">
                <button class="btn copy-btn" disabled><span class="ql-icon no-padding">📥</span> Copy from</button>
                <select id="copy-host">
                </select>
            </div>
            <button class="btn save-btn" disabled><span class="ql-icon no-padding">💾</span> Save</button>
        </div>
        <div class="cred-table">
            <div class="th">
                <div class="td">Username</div>
                <div class="td">Password</div>
                <div class="td"></div>
            </div>
            <div class="rows-container"></div>
        </div>
      </div>`;

    this._el = createElementFromHtml(settingsHtml);
    this._rowsContainer = this._el.querySelector('.cred-table .rows-container');
    this._addBtn = this._el.querySelector('.controls .add-btn');
    this._saveBtn = this._el.querySelector('.controls .save-btn');
    this._copyBtn = this._el.querySelector('.controls .copy-btn');
    this._copyHostSelect = this._el.querySelector('.controls #copy-host');
    this._utils = new CredentialsTableUtils(this);
  }

  async init() {
    const eventCallback = () => {
      this._utils.enableSaveBtn();
      this._utils.checkTableIsEmpty();
    }

    document.addEventListener('ql-config-changed', eventCallback);

    this._initAddBtn();
    this._initSaveBtn();
    await this._initCopyBtn();

    return this._el;
  }

  async toggle() {
    const styles = getComputedStyle(this._el);
    if (styles.getPropertyValue('opacity') === '0') {
      this._el.style.display = 'flex';
      await this._utils.refreshTable();
      await this._initCopyFunc();

      this._el.style.opacity = '1';
    } else {
      this._hide();
    }
  }

  _hide() {
    this._el.style.opacity = '0';
    setTimeout(() => this._el.style.display = 'none', 100);
  }

  async _refreshCopySelect() {
    const config = await chromeStorage.get('ql-config') || [];
    const hosts = new Set();
    config.forEach(entry => {
      if (entry.host !== window.location.host) hosts.add(entry.host);
    });

    this._copyHostSelect.innerHTML = '';
    this._copyHostSelect.appendChild(new Option('---', '', true, true));
    this._copyBtn.setAttribute('disabled', 'true');

    hosts.forEach(host => this._copyHostSelect.appendChild(new Option(host, host)));
  }

  _initAddBtn() {
    this._addBtn.addEventListener('click', () => this._utils.addRow('', ''));
  }

  _initSaveBtn() {
    this._saveBtn.addEventListener('click', async () => {
      let config = await chromeStorage.get('ql-config') || [];
      config = config.filter(item => item.host !== window.location.host); // remainder of config except current host
      const rows = this._rowsContainer.children;
      let isValid = true;

      for (let i = 0; i < rows.length; i++) {
        if (rows[i].classList.contains('only-text')) break;

        const host = window.location.host;
        const login = rows[i].querySelector('[data-field="login"]').value.trim();
        const password = rows[i].querySelector('[data-field="password"]').value;

        if (!this._utils.validateRow(rows[i])) {
          isValid = false;
        } else {
          config.push({ host, login, password });
        }
      }

      if (isValid) {
        await chromeStorage.set('ql-config', config);
        document.dispatchEvent(new Event('ql-config-changes-applied'));
        this._hide();
      }
    });
  }

  async _initCopyFunc() {
    await this._refreshCopySelect();

    this._copyHostSelect.addEventListener('change', e => {
      if (e.target.value !== '') {
        this._copyBtn.removeAttribute('disabled');
      } else {
        this._copyBtn.setAttribute('disabled', 'true');
      }
    });
  }

  async _initCopyBtn() {
    this._copyBtn.addEventListener('click', async () => {
      const selectedHost = this._copyHostSelect.value;
      let config = await chromeStorage.get('ql-config') || [];
      config = config.filter(item => item.host === selectedHost);

      config.forEach(item => this._utils.addRow(item.login, item.password));
      this._copyHostSelect.selectedIndex = 0;
      this._copyBtn.setAttribute('disabled', 'true');
    });
  }
}
