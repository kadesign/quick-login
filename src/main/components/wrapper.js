import chromeStorage from '../../utils/chromeStorage';
import Button from './button';
import Settings from './settings';

export default class AppWrapper {
  constructor() {
    this._el = document.createElement('div');
    this._el.classList.add('ql-wrapper');
    this._el.insertAdjacentHTML('afterbegin', '<div class="ql-buttons"></div>');

    this._settings = new Settings();
  }

  async init() {
    document.querySelector('body').appendChild(this._el);
    await this._createButtons();
    this._el.appendChild(await this._settings.init());

    document.addEventListener('ql-config-changes-applied', async () => {
      await this._createButtons();
    });
    document.addEventListener('ql-toggle-settings', () => {
      this._settings.toggle();
    });
  }

  async _createButtons() {
    const buttonsWrapper = this._el.querySelector('.ql-buttons');
    buttonsWrapper.innerHTML = '';

    const config = await chromeStorage.get('ql-config');

    if (config) {
      config.filter(item => item.host === window.location.host).forEach(item => {
        buttonsWrapper.appendChild(new Button(false, item.login, item.password).el);
      });
    }
    buttonsWrapper.appendChild(new Button(true).el);
  }
}
