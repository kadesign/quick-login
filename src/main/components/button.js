import { createElementFromHtml } from '../../utils/htmlConverter';

export default class Button {
  constructor(isSettingsBtn, login = null, password = null) {
    this._el = null;
    if (isSettingsBtn) {
      this._el = createElementFromHtml('<div class="ql-button ql-settings"><span class="ql-icon no-padding">⚙️</span></div>');
      this._initSettingsBtn();
    } else {
      this._el = createElementFromHtml(`<div class="ql-button" data-login="${login}" data-pw="${password}"><span class="ql-icon">🔑‍</span>${login}</div>`);
      this._initLoginBtn();
    }
  }

  get el() {
    return this._el;
  }

  _initSettingsBtn() {
    this._el.addEventListener('click', () => {
      document.dispatchEvent(new Event('ql-toggle-settings'));
    });
  }

  _initLoginBtn() {
    const usernameInput = document.getElementById('user_login');
    const passwordInput = document.getElementById('login_user_password');
    const loginButton = document.getElementById('login-jsp-btn');

    if (!this._el.hasAttribute('data-init')) {
      this._el.addEventListener('click', e => {
        let target = e.target;
        if (e.target.classList.contains('ql-icon')) target = e.path[1];
        usernameInput.value = target.getAttribute('data-login');
        passwordInput.value = target.getAttribute('data-pw');
        loginButton.click();
      });
      this._el.setAttribute('data-init', 'true');
    }
  }
}
