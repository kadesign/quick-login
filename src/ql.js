import './styles/ql.scss';
import AppWrapper from './main/components/wrapper';

const main = async () => {
  const wrapper = new AppWrapper();

  if (document.querySelector('.d-login-form')) {
    await wrapper.init();
  } else {
    new MutationObserver(async (mutationsList, observer) => {
      for (const mutation of mutationsList) {
        if (mutation.type === 'childList' && document.querySelector('.d-login-form')) {
          observer.disconnect();
          await wrapper.init();
        }
      }
    }).observe(document.querySelector('body'), { childList: true, subtree: true });
  }
};

main().catch(e => console.error(e));
