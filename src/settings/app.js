import SettingsTable from './components/settingsTable';

const main = async () => {
  const settingsTable = new SettingsTable();
  await settingsTable.init();
};

main().catch(e => console.error(e));