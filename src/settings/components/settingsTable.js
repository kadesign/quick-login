import chromeStorage from '../../utils/chromeStorage';
import CredentialsTableUtils from '../../shared/credentialsTableUtils';

export default class SettingsTable {
  constructor() {
    this._el = document.querySelector('.settings-table');
    this._rowsContainer = this._el.querySelector('.cred-table .rows-container');
    this._addBtn = this._el.querySelector('.controls .add-btn');
    this._saveBtn = this._el.querySelector('.controls .save-btn');
    this._utils = new CredentialsTableUtils(this);
  }

  async init() {
    const eventCallback = () => {
      this._utils.enableSaveBtn();
      this._utils.checkTableIsEmpty();
    }

    document.addEventListener('ql-config-changed', eventCallback);

    this._initAddBtn();
    this._initSaveBtn();
    await this._utils.refreshTable(true);

    return this._el;
  }

  _initAddBtn() {
    this._addBtn.addEventListener('click', () => this._utils.addRow('', '', ''));
  }

  _initSaveBtn() {
    this._saveBtn.addEventListener('click', async () => {
      const config = [];
      const rows = this._rowsContainer.children;
      let isValid = true;

      for (let i = 0; i < rows.length; i++) {
        if (rows[i].classList.contains('only-text')) break;

        const host = rows[i].querySelector('[data-field="host"]').value.trim();
        const login = rows[i].querySelector('[data-field="login"]').value.trim();
        const password = rows[i].querySelector('[data-field="password"]').value;

        if (!this._utils.validateRow(rows[i])) {
          isValid = false;
        } else {
          config.push({ host, login, password });
        }
      }

      if (isValid) {
        await chromeStorage.set('ql-config', config);
        this._disableSaveBtn();
        this._markSaveBtnSuccess();
      }
    });
  }

  _disableSaveBtn() {
    this._saveBtn.setAttribute('disabled', 'true');
  }

  _markSaveBtnSuccess() {
    this._saveBtn.innerHTML = '<span class="ql-icon no-padding">✔</span> Saved';
    setTimeout(() => this._saveBtn.innerHTML = '<span class="ql-icon no-padding">💾</span> Save', 2000);
  }
}