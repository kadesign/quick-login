<div align="center">
    <h1>QuickLogin</h1>
    <p>Add-on for BMC Remedy SSO login page to quickly log in with predefined credentials</p>
    <p>
        <img alt="Is maintained" src="https://img.shields.io/maintenance/yes/2021">
        <a href="https://gitlab.com/kadesign/quick-login/-/releases/v1.0.2"><img src="https://img.shields.io/badge/version-v1.0.2-blue" alt="Version"></a>
        <a href="https://gitlab.com/kadesign/quick-login/-/commits/master"><img alt="Pipeline status" src="https://gitlab.com/kadesign/quick-login/badges/master/pipeline.svg"></a>
        <a href="https://chrome.google.com/webstore/detail/kmdjfmhilmckjmmilekccppcemcakkph"><img alt="Chrome Web Store version" src="https://img.shields.io/chrome-web-store/v/kmdjfmhilmckjmmilekccppcemcakkph"></a>
        <img alt="Chrome Web Store downloads" src="https://img.shields.io/chrome-web-store/users/kmdjfmhilmckjmmilekccppcemcakkph">
        <img alt="Chrome Web Store rating" src="https://img.shields.io/chrome-web-store/stars/kmdjfmhilmckjmmilekccppcemcakkph">
    </p>
</div>

## Key features

- Configure credentials to BMC Remedy servers to quickly log in to them via SSO login page.

**⚠ Passwords are stored as plain text in Chrome extensions local storage. Do not use this extension for production and other sensitive environments!**

## How to install

### Chrome Web Store

The latest stable release can be downloaded and installed from Chrome Web Store.

**[Download](https://chrome.google.com/webstore/detail/kmdjfmhilmckjmmilekccppcemcakkph)**

### Manual installation

In case downloading from CWS doesn't fit for you, the extension can be installed manually.

*Please note that automatic updates aren't possible in this case.*

- Download the latest ZIP build from [Releases](https://gitlab.com/kadesign/quick-login/-/releases).
- Unzip the archive and place the directory `quick-login` to any desired location. Do not delete the directory unless you don't need the extension anymore!
- Go to Extensions page (`chrome://extensions`) and enable Developer mode.
- Drag the `quick-login` folder anywhere on the page to import the extension.

## How to use

The button "⚙" will appear on the SSO login page. Click it to configure credentials for current environment.

Open extension settings to see and change all configured credentials for all environments.

## Changelog

You can find full changelog [here](CHANGELOG.md).

## Bugs and feature requests

Have a bug or a feature request? Please first search for existing and closed issues. If your problem or idea is not addressed yet, [please open a new issue](https://gitlab.com/kadesign/quick-login/-/issues/new).

## Copyright and license

Copyright 2021 [Danil Katashev](mailto:dev@danilkatashev.ru). Released under the [MIT License](LICENSE).<br>
Icon made by [Freepik](https://www.freepik.com) from [Flaticon](https://www.flaticon.com).<br>
BMC, the BMC logo, and other BMC marks are assets of BMC Software, Inc. These trademarks are registered and may be registered in the U.S. and in other countries.
