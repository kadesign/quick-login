# Changelog

## 1.0.2 (2021-06-08)
**Improvements:**
- Extension will also work with login pages with following URL pattern: `<host>/rsso/start?<args>`.
- Credentials can be copied from one environment to another: just select source environment on the configuration popup and click "Copy from". Don't forget to save changes afterwards! 

**Technical improvements:**
- Code is refactored for better maintainability
- Gitlab pipeline is tweaked to keep artifacts only for releases

## 1.0.1 (2021-05-09)
**Tiny but important bugfix:**
- Fixed extension configuration being overwritten when saving user configuration for particular host.

## 1.0.0 (2021-05-08)
✨ **Initial release of QuickLogin!**

🚀 **Features:**
- Configure credentials to BMC Remedy servers to quickly log in to them via SSO login page.
